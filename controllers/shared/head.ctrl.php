<?php

class SharedHeadController extends Controller
{
	const REVISION = 31;

	public function build( )
	{
        $link = "";

        $info = $this->getParams();
        if(isset($info["key_main_controller"])) {
            $link = $info["key_main_controller"];
            $this->assign( 'link', $link );
            $this->assign( 'revision', self::REVISION );
            $this->setLayout( 'shared/head.tpl' );
        }
        else {
            $this->assign( 'link', $link );
            $this->assign( 'revision', self::REVISION );
            $this->setLayout( 'shared/head.tpl' );
        }
	}
}


?>
