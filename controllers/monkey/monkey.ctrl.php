<?php

/*
 * Monkey Controller:
 * S'encarrega de mostrar la pàgina amb el mico corresponent.
 */
class MonkeyMonkeyController extends Controller
{
    protected $view = 'monkey/monkey.tpl';
    private $img;

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {
        $error = false;

        // Agafem els paràmetres
        $info = $this->getParams();

        if(isset($info["url_arguments"])) {
            $monkeyNumber = $info["url_arguments"][0];

            // Si hi han més d'un paràmetre hi ha error 404 sino seguim
            if(sizeof($info["url_arguments"]) == 1) {
                if($info["url_arguments"][0] == "") {
                    $this->assign('img',"monkey1");
                    $this->assign('numero',1);
                    $this->assign('anterior',0);
                    $this->assign('seguent',2);
                    $this->setLayout($this->view);
                }
                else {
                    // Si el mico no està a la galeria hi ha error 404 sino mostrem el mico
                    if($monkeyNumber >= 1 && $monkeyNumber <= 10) {
                        $img = $info["key_main_controller"].$monkeyNumber;
                        $this->assign('img',$img);
                        $this->assign('numero',$monkeyNumber);
                        $this->assign('anterior',$monkeyNumber-1);
                        $this->assign('seguent',$monkeyNumber+1);
                        $this->setLayout($this->view);
                    }
                    else $error = true;
                }
            }
            else {
                if(sizeof($info["url_arguments"]) == 2 && $info["url_arguments"][1] == ""){
                    $img = $info["key_main_controller"].$monkeyNumber;
                    $this->assign('img',$img);
                    $this->assign('numero',$monkeyNumber);
                    $this->assign('anterior',$monkeyNumber-1);
                    $this->assign('seguent',$monkeyNumber+1);
                    $this->setLayout($this->view);
                }
                else $error = true;
            }

            // Si hi ha error
            if($error)
                $this->setLayout('error/error404.tpl');
        }
        else {
            $this->assign('img',"monkey1");
            $this->assign('numero',1);
            $this->assign('anterior',0);
            $this->assign('seguent',2);
            $this->setLayout($this->view);
        }


    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        return $modules;
    }
}