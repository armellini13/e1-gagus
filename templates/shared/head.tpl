<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="title" content="" />
	<meta name="robots" content="all" />
	<meta name="expires" content="never" />
	<meta name="distribution" content="world" />
    <script src="{$url.global}/js/jquery-1.10.2.min.js"></script>
	<title>Monkey Gallery</title>
	<link rel="stylesheet" href="{$url.global}/css/style.css">
</head>
<body id="home">
    <div id='cssmenu'>
        <ul>
        {   if $link == "home" || $link == "default"}
            <li class='active'><a href='{$url.global}/home'><span>Home</span></a></li>
        {   else}
            <li><a href='{$url.global}/home'><span>Home</span></a></li>
        {   /if}
        {   if $link == "monkey"}
            <li class='active'><a href='{$url.global}/monkey/1'><span>Gallery</span></a></li>
        {   else}
            <li><a href='{$url.global}/monkey/1'><span>Gallery</span></a></li>
        {   /if}
        {   if $link == "about"}
            <li class='active'><a href='{$url.global}/about'><span>About</span></a></li>
        {   else}
            <li><a href='{$url.global}/about'><span>About</span></a></li>
        {   /if}
        </ul>
    </div>
	<div id="wrapper">