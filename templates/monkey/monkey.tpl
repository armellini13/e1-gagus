{$modules.head}

<!-- Això és un comentari HTML -->
{* Això és un comentari en Smarty *}

<div class="block">
    <div id="inner-block" align="center">
        <h2 id="monkeyNumber">Monkey {$numero}</h2>
        <img src="{$url.global}/imag/{$img}.jpg" >
        <br><br>
    {   if $numero != 1}
        <a href="{$url.global}/monkey/{$anterior}">
            Anterior
        </a>
    {   else}
        <a href="{$url.global}/monkey/10">
            Anterior
        </a>
    {   /if}
    {   if $numero != 10}
        <a href="{$url.global}/monkey/{$seguent}">
            Següent
        </a>
    {   else}
        <a href="{$url.global}/monkey/1">
            Següent
        </a>
    {   /if}
        <br><br>
    </div>
</div>

<div class="clear"></div>
{$modules.footer}